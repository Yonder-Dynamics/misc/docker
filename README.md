# Docker setup for simulation and everything

`docker-compose.yml` is a set of common services that are used in both of our
environments.

`rover.yml` sets up the environment as it is on the rover.

`simulation.yml` is the simulation environment.

IMPORTANT: Only commit changes to this repo if they have been tested on the
rover, since this is our source of truth.

## Installation

First install docker and docker-compose

To clone use `git clone --recursive` because we have submodules

for first time run `source run_simulation.sh --build` and just leave it for like an hour

then further times `source run_simulation.sh`

To restart+recompile gazebo only run `docker-compose up -d --force-recreate --no-deps --build gazebo` or frontend or whichever u want. Do this in another terminal that is also at the root of this repo

To switch between worlds edit the world variable at the end of gazebo/Dockerfile. It should match one the world files in gazebo/roversimulation/worlds

localhost:3000 for the website
localhost:8080 for the gzweb (gazebo)

## Debugging

To attach and see whats going on in docker run `docker run -it --network=ros-network ros:melodic`
Then enter `ROS_MASTER_URI=http://docker_roscore_1:11311` and you can see whats going on

### Recording Video

To record video `sudo apt-get install ros-melodic-image-view`
Then `mkdir temp && cd temp && rosrun image_view image_saver image:=/rover_simulation/body_camera/color/image_raw`
then run `docker cp angry_keldysh:/temp ~/Desktop` replacing angry_keldysh with your container name (`container ls`)

Finally within gazebo folder run `python videoConverter.py [path] [highest_image_number]` and ill generate a video

<http://gazebosim.org/gzweb.html#install-collapse-1>
<http://gazebosim.org/gzweb.html#install-collapse-2>

### Include Body camera in recording

```bash
docker run -it --network=ros-network -p 8081:8080 ros:melodic
ROS_MASTER_URI=http://docker_roscore_1:11311
mkdir -p /catkin_ws/src/ && cd /catkin_ws && catkin_make
git clone https://github.com/RobotWebTools/web_video_server/ src/web_video_server
. /catkin_ws/devel/setup.sh && sudo apt update && rosdep install web_video_server
catkin_make && . /catkin_ws/devel/setup.sh
rosrun web_video_server web_video_server
```

Then open up localhost:8081 and point the image in the frontend towards one of the streams
