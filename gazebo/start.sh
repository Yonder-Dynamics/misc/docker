sleep 10
echo building $WORLD
# Starts a virtual xserver
rm /tmp/.X1-lock
Xvfb :1 -screen 0 1600x1200x16  &
export DISPLAY=:1.0
. /catkin_ws/devel/setup.sh && roslaunch rover_simulation start_headless.launch world:=$WORLD &
. /usr/share/gazebo/setup.sh && . ~/.bashrc && . ~/.nvm/nvm.sh && cd /root/gzweb/ && npm start
