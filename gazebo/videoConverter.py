#%%
import cv2
import numpy as np
import sys

if len(sys.argv) != 3:
    print("python videoConverter.py [path] [highest_image_number]")

path = sys.argv[1]
highest_number = int(sys.argv[2])
fourcc = cv2.VideoWriter_fourcc(*"mp4v")

img = []
for i in range(1, highest_number + 1):
    img.append(cv2.imread(path + "/left" + str(i).zfill(4) + ".jpg"))

height, width, layers = img[0].shape

video = cv2.VideoWriter("video.avi", fourcc, 15, (width, height))

for i in range(0, highest_number):
    video.write(img[i])

cv2.destroyAllWindows()
video.release()
# %%
